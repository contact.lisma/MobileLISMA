import { NavigationContainer } from "@react-navigation/native";
import React, { useState } from "react";
import Navigation from "./src/navigation/Navigation";
import {
  codeContext,
  roleContext,
  userContext,
  userIdContext,
} from "./src/utils/context";

export default function App() {
  const [user, setUser] = useState("");
  const [userId, setUserId] = useState(0);
  const [roleUser, setRoleUser] = useState("");
  const [codeUser, setCodeUser] = useState(0);

  return (
    <userContext.Provider value={{ user, setUser }}>
      <userIdContext.Provider value={{ userId, setUserId }}>
        <roleContext.Provider value={{ roleUser, setRoleUser }}>
          <codeContext.Provider value={{ codeUser, setCodeUser }}>
            <NavigationContainer>
              <Navigation></Navigation>
            </NavigationContainer>
          </codeContext.Provider>
        </roleContext.Provider>
      </userIdContext.Provider>
    </userContext.Provider>
  );
}
