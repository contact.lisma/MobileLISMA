import React from "react";
import Home from "../screens/Home";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { AntDesign, Entypo, Ionicons } from "@expo/vector-icons";
import Patients from "../screens/Patients";
import Contacts from "../screens/Contacts";
import Params from "./Params";
import useRole from "../hooks/role";

const Tab = createBottomTabNavigator();

export default function LoggedClient() {
  const { roleUser, setRoleUser } = useRole();

  if (roleUser == "patient") {
    return (
      <Tab.Navigator
        initialRouteName="Home"
        screenOptions={{ headerShown: false }}
      >
        <Tab.Screen
          name="Home"
          component={Home}
          options={{
            tabBarIcon: ({ focused, color, size }) => {
              return <Entypo name="folder" size={24} color="#15cdfc" />;
            },
          }}
        />
        <Tab.Screen
          name="Params"
          component={Params}
          options={{
            tabBarIcon: ({ focused, color, size }) => {
              return <Ionicons name="options" size={24} color="#15cdfc" />;
            },
          }}
        />
      </Tab.Navigator>
    );
  }

  if (roleUser == "mentor") {
    return (
      <Tab.Navigator
        initialRouteName="Home"
        screenOptions={{ headerShown: false }}
      >
        <Tab.Screen
          name="Home"
          component={Home}
          options={{
            tabBarIcon: ({ focused, color, size }) => {
              return <Entypo name="home" size={24} color="#15cdfc" />;
            },
          }}
        />
        <Tab.Screen
          name="Contacts"
          component={Contacts}
          options={{
            tabBarIcon: ({ focused, color, size }) => {
              return (
                <Ionicons name="ios-person-circle" size={24} color="#15cdfc" />
              );
            },
          }}
        />
        <Tab.Screen
          name="Params"
          component={Params}
          options={{
            tabBarIcon: ({ focused, color, size }) => {
              return <Ionicons name="options" size={24} color="#15cdfc" />;
            },
          }}
        />
      </Tab.Navigator>
    );
  }
}
