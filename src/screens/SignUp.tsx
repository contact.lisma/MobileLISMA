import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { ScrollView, Text, StyleSheet, ToastAndroid } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import CustomButton from "../components/CustomButton";
import CustomInput from "../components/CustomInput";
import { COLORS } from "../utils/colors";
import { Picker } from "@react-native-picker/picker";

export default function SignUp() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [pwd, setPwd] = useState("");
  const [verifPwd, setVerifPwd] = useState("");
  const [send, setSend] = useState(false);
  const [total, setTotal] = useState(0);
  const [role, setRole] = useState("patient");

  const options = ["patient", "mentor"];

  const navigation = useNavigation();

  useEffect(() => {
    getTotalUser();
  }, []);

  const onSignUpPressed = () => {
    verifyMdp();
    if (send) {
      getTotalUser();
      //@ts-ignore
      navigation.navigate("SignIn");
    }
  };

  const verifyMdp = () => {
    if (pwd === verifPwd) {
      postUsers();
    } else {
      ToastAndroid.show(
        JSON.stringify("Les mots de passe ne sont pas identiques"),
        ToastAndroid.SHORT
      );
    }
    getTotalUser();
  };

  const onSignInPressed = () => {
    //@ts-ignore
    navigation.navigate("SignIn");
  };

  const onTermsPressed = () => {
    console.log("Terms");
  };

  const onPrivacyPressed = () => {
    console.log("Privacy");
  };

  const getTotalUser = async () => {
    var getUsers = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Utilisateurs/",
    };

    await axios(getUsers)
      .then(function (response) {
        setTotal(response.data.meta.pagination.total);
        setTotal((old) => old + 1);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const postUsers = async () => {
    var postUserMessage = JSON.stringify({
      data: {
        nom: username,
        email: email,
        password: pwd,
        code: total,
        role: role,
      },
    });

    var postOneUser = {
      method: "post",
      url: "https://server-api-lisma.herokuapp.com/api/Utilisateurs/",
      headers: {
        "Content-Type": "application/json",
      },
      data: postUserMessage,
    };

    await axios(postOneUser)
      .then(function (response) {
        setSend(true);
        ToastAndroid.show(
          JSON.stringify(
            "Bonjour " + username + " votre compte a bien été créé"
          ),
          ToastAndroid.SHORT
        );
      })
      .catch(function (error) {
        setSend(false);
        ToastAndroid.show(
          JSON.stringify(error.response.data.error.message),
          ToastAndroid.SHORT
        );
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.title}>Créer un compte</Text>
        <CustomInput
          values={username}
          setValue={setUsername}
          placeholder={"Entrez votre nom"}
          secureTextEntry={false}
        ></CustomInput>
        <CustomInput
          values={email}
          setValue={setEmail}
          placeholder={"Entrez votre email"}
          secureTextEntry={false}
        ></CustomInput>
        <CustomInput
          values={pwd}
          setValue={setPwd}
          placeholder={"Entrez votre mot de passe"}
          secureTextEntry={true}
        ></CustomInput>
        <CustomInput
          values={verifPwd}
          setValue={setVerifPwd}
          placeholder={"Entrez une nouvelle fois votre mot de passe"}
          secureTextEntry={true}
        ></CustomInput>

        <Text>Choissisez votre rôle : </Text>

        <Picker
          itemStyle={{
            backgroundColor: "white",
            color: "black",
            borderColor: "rgba(0,0,0,0.2)",
            fontSize: 16,
          }}
          mode="dropdown"
          selectedValue={role}
          onValueChange={(value) => {
            setRole(value);
          }}
        >
          {options.map((item, index) => {
            return <Picker.Item label={item} value={item} key={index} />;
          })}
        </Picker>

        <CustomButton
          text="Créer un compte"
          onPress={onSignUpPressed}
        ></CustomButton>
        <Text style={styles.text}>
          By registering, you confirm that you accept our{" "}
          <Text style={styles.link} onPress={onTermsPressed}>
            Terms of Use
          </Text>{" "}
          and{" "}
          <Text style={styles.link} onPress={onPrivacyPressed}>
            Privacy Police
          </Text>
        </Text>
        <CustomButton
          text="Vous avez un compte ? Connectez vous !"
          onPress={onSignInPressed}
          type="TERTIARY"
        ></CustomButton>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: COLORS.title,
    margin: 10,
    textAlign: "center",
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
});
