import { getActionFromState, useNavigation } from "@react-navigation/native";
import axios from "axios";
import React, { useEffect } from "react";
import { useState } from "react";
import {
  Text,
  StyleSheet,
  ToastAndroid,
  ScrollView,
  RefreshControl,
  View,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import CustomButton from "../components/CustomButton";
import CustomInput from "../components/CustomInput";
import useCode from "../hooks/code";
import useUserId from "../hooks/id";
import useRole from "../hooks/role";
import useUser from "../hooks/user";
import { Users } from "../utils/users";
import AsyncStorage from "@react-native-async-storage/async-storage";

const wait = (timeout: number | undefined) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

export default function Params() {
  const { user, setUser } = useUser();
  const { userId, setUserId } = useUserId();
  const { roleUser, setRoleUser } = useRole();
  const { codeUser, setCodeUser } = useCode();
  const navigation = useNavigation();

  const [getUsers, setGetUsers] = useState([] as Users[]);
  const [code, setCode] = useState("");
  const [amiName, setAmiName] = useState("");
  const [amiId, setAmiId] = useState(0);
  const [setRefresh, refresh] = useState(false);

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
    getAllUsers();
  }, []);

  useEffect(() => {
    getAllUsers();
  }, []);

  const getAllUsers = async () => {
    var getUsers = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Utilisateurs/",
    };

    await axios(getUsers)
      .then(function (response) {
        setGetUsers(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const getAmiName = () => {
    getUsers.map((ami) => {
      if (ami.attributes.code.toString() == code) {
        <Text key={ami.id}></Text>;
        setAmiName(ami.attributes.nom);
        setAmiId(ami.id);
        onAddFriend();
      }
    });
  };

  const onAddFriend = async () => {
    var postFriendMessage = JSON.stringify({
      data: {
        id_mentor: userId,
        id_patient: amiId,
      },
    });

    var postOneFriend = {
      method: "post",
      url: "https://server-api-lisma.herokuapp.com/api/Amis/",
      headers: {
        "Content-Type": "application/json",
      },
      data: postFriendMessage,
    };
    await axios(postOneFriend)
      .then(function (response) {
        ToastAndroid.show(
          JSON.stringify("Votre ami " + amiName + " a bien été ajouté !"),
          ToastAndroid.SHORT
        );
      })
      .catch(function (error) {
        ToastAndroid.show(
          JSON.stringify(error.response.data.error.message),
          ToastAndroid.SHORT
        );
      });
  };

  const onDeconnect = async () => {
    setUser("");
    AsyncStorage.clear();
    //@ts-ignore
    navigation.navigate("SignIn");
  };

  if (roleUser == "patient") {
    return (
      <SafeAreaView
        style={(styles.container, { flex: 1, justifyContent: "center" })}
      >
        <Text style={{ textAlign: "center", marginBottom: 10 }}>
          Bonjour {user} que voulez vous faire ?{" "}
        </Text>

        <View
          style={{
            borderColor: "#15cdfc",
            borderWidth: 2,
            marginTop: 20,
            borderRadius: 20,
            padding: 10,
          }}
        >
          <Text style={{ textAlign: "center" }}>
            Code pour l'ajout d'ami : {codeUser}
          </Text>
        </View>
        <CustomButton
          text="Se déconnecter"
          onPress={onDeconnect}
        ></CustomButton>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          contentContainerStyle={styles.scrollView}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          <Text style={{ marginBottom: 10 }}>
            Bonjour {user} que voulez vous faire ?{" "}
          </Text>

          <Text>Entrez le code de l'ami que vous voulez ajouter :</Text>
          <CustomInput
            values={code}
            setValue={setCode}
            placeholder={"Entrez le code"}
            secureTextEntry={false}
          ></CustomInput>

          <CustomButton
            text="Ajouter un utilisateur"
            onPress={getAmiName}
          ></CustomButton>
          <View
            style={{
              borderColor: "#15cdfc",
              borderWidth: 2,
              marginTop: 20,
              marginBottom: 20,
              borderRadius: 20,
              padding: 10,
              alignContent: "center",
            }}
          >
            <Text style={{ marginBottom: 10, textAlign: "center" }}>
              Liste des noms avec leur code:{" "}
            </Text>
            {getUsers.map((user) => {
              if (user.id != userId && user.attributes.role == "patient") {
                return (
                  <Text key={user.id}>
                    Le code de {user.attributes.nom} est {user.attributes.code}
                  </Text>
                );
              }
            })}
          </View>
          <CustomButton
            text="Se déconnecter"
            onPress={onDeconnect}
          ></CustomButton>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  scrollView: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
