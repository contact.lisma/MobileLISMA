import axios from "axios";
import React, { Component, useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Button,
  DevSettings,
  ScrollView,
  RefreshControl,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import useUserId from "../hooks/id";
import { Ami } from "../utils/ami";
import { Users } from "../utils/users";

const wait = (timeout: number | undefined) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

export default function Patients() {
  const { userId, setUserId } = useUserId();

  const [amis, setAmis] = useState([] as Ami[]);
  const [getUsers, setGetUsers] = useState([] as Users[]);
  const [idSupprFriend, setIdSupprFriend] = useState(0);

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    getAllAmis();
    getAllUsers();
  }, []);

  useEffect(() => {
    getAllAmis();
    getAllUsers();
  }, [amis, idSupprFriend]);

  const getAllUsers = async () => {
    var getUsers = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Utilisateurs/",
    };

    await axios(getUsers)
      .then(function (response) {
        setGetUsers(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const getAllAmis = async () => {
    var getUsers = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Amis/",
    };

    await axios(getUsers)
      .then(function (response) {
        setAmis(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const onSupprFriend = () => {
    amis.map((ami) => {
      if (ami.attributes.id_mentor == userId) {
        getUsers.map((user) => {
          if (user.id == ami.attributes.id_patient) {
            setIdSupprFriend((old) => ami.id);
            supprFriend();
            getAllAmis();
            getAllUsers();
          }
        });
      }
    });
  };

  const supprFriend = () => {
    var config = {
      method: "delete",
      url: "https://server-api-lisma.herokuapp.com/api/Amis/" + idSupprFriend,
      headers: {},
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {amis.map((ami) => {
          if (ami.attributes.id_mentor == userId) {
            return getUsers.map((user) => {
              if (user.id == ami.attributes.id_patient) {
                return (
                  <View
                    style={{
                      borderColor: "#15cdfc",
                      borderWidth: 2,
                      marginTop: 20,
                      borderRadius: 20,
                      padding: 10,
                    }}
                  >
                    <Text key={user.id}>
                      Votre ami {user.attributes.nom} est dans vos contact que
                      souhaitez vous faire ?
                    </Text>
                    <Button
                      onPress={onSupprFriend}
                      title="Supprimer l'ami"
                      color={"#15cdfc"}
                    ></Button>
                  </View>
                );
              }
            });
          }
        })}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  scrollView: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
