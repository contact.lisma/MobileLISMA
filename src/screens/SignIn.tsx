import React, { useEffect, useState } from "react";
import {
  ScrollView,
  Image,
  StyleSheet,
  useWindowDimensions,
  ToastAndroid,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
// @ts-ignore
import Logo from "../../assets/images/LogoLisma.png";
import CustomButton from "../components/CustomButton";
import CustomInput from "../components/CustomInput";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import { Users } from "../utils/users";
import useUser from "../hooks/user";
import useUserId from "../hooks/id";
import useRole from "../hooks/role";
import useCode from "../hooks/code";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function SignIn() {
  const { user, setUser } = useUser();
  const { userId, setUserId } = useUserId();
  const { roleUser, setRoleUser } = useRole();
  const { codeUser, setCodeUser } = useCode();

  const [username, setUsername] = useState("");
  const [pwd, setPwd] = useState("");
  const [getUsers, setGetUsers] = useState([] as Users[]);

  const { height } = useWindowDimensions();
  const navigation = useNavigation();

  useEffect(() => {
    verifyStorage();
    getAllUsers();
  }, []);

  const verifyStorage = () => {
    _retrieveData();
    onSignInPressed();
  };

  const _retrieveData = async () => {
    try {
      const valueUsername = await AsyncStorage.getItem("username");
      const valuePwd = await AsyncStorage.getItem("pwd");
      if (valueUsername !== null && valuePwd !== null) {
        setUsername(() => valueUsername);
        setPwd(() => valuePwd);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const onSignInPressed = () => {
    let found = false;

    setUsername(username.trim());

    getUsers.map((users) => {
      if (
        users.attributes.nom == username &&
        users.attributes.password == pwd
      ) {
        found = true;
        setUser(username);
        setUserId(users.id);
        setRoleUser(users.attributes.role);
        setCodeUser(users.attributes.code);
        _storeData();
        ToastAndroid.show("Bienvenue " + username, ToastAndroid.SHORT);
        //@ts-ignore
        navigation.navigate("LoggedClient");
      }
      if (found == false) {
        ToastAndroid.show(
          "Nom d'utilisateur ou mot de passe incorrect",
          ToastAndroid.SHORT
        );
      }
    });
  };

  const _storeData = async () => {
    try {
      await AsyncStorage.setItem("username", username);
      await AsyncStorage.setItem("pwd", pwd);
    } catch (error) {
      console.log(error);
    }
  };

  const onForgotPasswordPressed = () => {
    //@ts-ignore
    navigation.navigate("MdpForget");
  };

  const onCreateAccount = () => {
    //@ts-ignore
    navigation.navigate("SignUp");
  };

  const getAllUsers = async () => {
    var getUsers = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Utilisateurs/",
    };

    await axios(getUsers)
      .then(function (response) {
        setGetUsers(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Image
          source={Logo}
          style={[styles.logo, { height: height * 0.3 }]}
          resizeMode="contain"
        ></Image>
        <CustomInput
          values={username}
          setValue={setUsername}
          placeholder={"Entrez votre nom"}
          secureTextEntry={false}
        ></CustomInput>
        <CustomInput
          values={pwd}
          setValue={setPwd}
          placeholder={"Entrez votre mot de passe"}
          secureTextEntry={true}
        ></CustomInput>
        <CustomButton
          text="S'identifier"
          onPress={onSignInPressed}
        ></CustomButton>
        <CustomButton
          text="Mot de passe oublié ? "
          onPress={onForgotPasswordPressed}
          type="TERTIARY"
        ></CustomButton>
        <CustomButton
          text="Vous n'avez pas de compte ? Créez en un !"
          onPress={onCreateAccount}
          type="TERTIARY"
        ></CustomButton>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  logo: {
    width: "70%",
    maxWidth: 300,
    alignSelf: "center",
  },
});
