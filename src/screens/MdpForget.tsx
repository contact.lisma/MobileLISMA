import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { ScrollView, Text, StyleSheet } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import CustomButton from "../components/CustomButton";
import CustomInput from "../components/CustomInput";
import { COLORS } from "../utils/colors";

export default function MdpForget() {
  const [username, setUsername] = useState("");

  const navigation = useNavigation();

  const onSignInPressed = () => {
    //@ts-ignore
    navigation.navigate("SignIn");
  };

  const onSendPressed = () => {
    //@ts-ignore
    navigation.navigate("NewPassword");
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.title}>Mot de passe oublié</Text>

        <CustomInput
          values={username}
          setValue={setUsername}
          placeholder={"Entrez votre nom d'utilisateur"}
          secureTextEntry={false}
        ></CustomInput>

        <CustomButton text="Envoyer" onPress={onSendPressed}></CustomButton>

        <CustomButton
          text="Revenir à la connexion"
          onPress={onSignInPressed}
          type="TERTIARY"
        ></CustomButton>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: COLORS.title,
    margin: 10,
    textAlign: "center",
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
});
