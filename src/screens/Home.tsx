import React, { useEffect, useState } from "react";
import {
  Alert,
  BackHandler,
  Button,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  View,
} from "react-native";
import axios from "axios";
import { SafeAreaView } from "react-native-safe-area-context";
import useUser from "../hooks/user";
import useUserId from "../hooks/id";
import useRole from "../hooks/role";
import { Medicaments } from "../utils/medicament";
import { Ordonnance } from "../utils/ordonnance";
import { Users } from "../utils/users";
import { Notifications } from "../utils/notifications";
import { Ami } from "../utils/ami";
import { useNavigation } from "@react-navigation/native";

const wait = (timeout: number | undefined) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

export default function Home() {
  const { user, setUser } = useUser();
  const { roleUser, setRoleUser } = useRole();
  const { userId, setUserId } = useUserId();

  const [getUsersId, setGetUsersId] = useState([] as Users[]);
  const [medicaments, setMedicaments] = useState([] as Medicaments[]);
  const [ordonnance, setOrdonnance] = useState([] as Ordonnance[]);
  const [notifications, setNotifications] = useState([] as Notifications[]);
  const [amis, setAmis] = useState([] as Ami[]);

  const [titre, setTitre] = useState("");
  const [idClient, setIdClient] = useState(0);
  const [contenu, setContenu] = useState("");

  const [refreshing, setRefreshing] = React.useState(false);

  const navigation = useNavigation();

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
    onGetMedicamentClients();
    onGetOrdonnance();
    setUserIdAfter();
    onGetNotifications();
    getAllAmis();
  }, []);

  useEffect(() => {
    const backAction = () => {
      Alert.alert(
        "Attention !",
        "Vous voulez vraiment quitter l'application ?",
        [
          {
            text: "Annuler",
            onPress: () => null,
            style: "cancel",
          },
          { text: "OUI", onPress: () => BackHandler.exitApp() },
        ]
      );
      return true;
    };
    getUserId();
    onGetMedicamentClients();
    onGetOrdonnance();
    setUserIdAfter();
    getAllAmis();
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const setUserIdAfter = () => {
    getUsersId.map((users) => {
      <Text key={users.id}></Text>;
      if (user == users.attributes.nom) {
        //@ts-ignore
        setUserId((old) => users.id);
      }
    });
  };

  const onSetValue = (
    nom_medicament: string,
    id_client: number,
    nbr_medicament: number,
    nom_client: string
  ) => {
    setTitre(
      () =>
        "Le médicament " + nom_medicament + " a bien été pris par " + nom_client
    );
    setIdClient(() => id_client);

    var jour = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var hour = new Date().getHours();
    var minutes = new Date().getMinutes();

    var heure = hour + ":" + minutes + "h";
    var date = jour + "/" + month;

    setContenu(
      () =>
        nom_client +
        " a bien pris ses " +
        nbr_medicament +
        " " +
        nom_medicament +
        " à " +
        heure +
        " le " +
        date
    );
    onAcceptMedicament();
  };

  const getAllAmis = async () => {
    var getUsers = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Amis/",
    };

    await axios(getUsers)
      .then(function (response) {
        setAmis(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const onGetNotifications = async () => {
    var onGetOneNotification = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Notifications/",
    };

    await axios(onGetOneNotification)
      .then(function (response) {
        setNotifications(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const getUserId = async () => {
    var getUserId = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Utilisateurs/",
    };

    await axios(getUserId)
      .then(function (response) {
        setGetUsersId(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const onGetMedicamentClients = async () => {
    var getMedicaments = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Medicaments/",
    };

    await axios(getMedicaments)
      .then(function (response) {
        setMedicaments(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const onGetOrdonnance = async () => {
    var onGetOrdonnance = {
      method: "get",
      url: "https://server-api-lisma.herokuapp.com/api/Ordonnances/",
    };

    await axios(onGetOrdonnance)
      .then(function (response) {
        setOrdonnance(response.data.data);
      })
      .catch(function (error) {
        console.log("get " + error);
      });
  };

  const onAcceptMedicament = async () => {
    var postAcceptMedicament = JSON.stringify({
      data: {
        titre: titre,
        id_client: idClient,
        contenu: contenu,
      },
    });

    var postOneNotification = {
      method: "post",
      url: "https://server-api-lisma.herokuapp.com/api/Notifications/",
      headers: {
        "Content-Type": "application/json",
      },
      data: postAcceptMedicament,
    };
    await axios(postOneNotification)
      .then(function (response) {
        ToastAndroid.show(
          JSON.stringify("Medicament bien pris !"),
          ToastAndroid.SHORT
        );
      })
      .catch(function (error) {
        ToastAndroid.show(
          JSON.stringify(error.response.data.error.message),
          ToastAndroid.SHORT
        );
      });
  };

  if (roleUser == "patient") {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.scrollView}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          <Text style={{ textAlign: "center", fontSize: 20, marginBottom: 20 }}>
            Bonjour {user}
          </Text>
          <Text>Voici la liste de vos médicaments :</Text>

          {ordonnance.map((ordonnance) => {
            if (userId == ordonnance.attributes.id_patient) {
              return (
                <View
                  style={{
                    borderColor: "#15cdfc",
                    borderWidth: 2,
                    marginTop: 20,
                    borderRadius: 20,
                    padding: 10,
                  }}
                >
                  <Text key={ordonnance.id} style={{ marginTop: 20 }}>
                    Votre medicament {ordonnance.attributes.nom_medicament} est
                    a prendre à {ordonnance.attributes.heure.substring(0, 5)}h
                    et il faut en prendre {ordonnance.attributes.nbr_medicament}{" "}
                    l'avez vous pris ?
                  </Text>
                  <Button
                    onPress={() =>
                      onSetValue(
                        ordonnance.attributes.nom_medicament,
                        ordonnance.attributes.id_patient,
                        ordonnance.attributes.nbr_medicament,
                        user
                      )
                    }
                    title="Oui"
                    color={"#15cdfc"}
                  ></Button>
                </View>
              );
            }
          })}
        </ScrollView>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.scrollView}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          <Text>Voici la liste des médicaments que vos amis ont pris :</Text>
          {amis.map((ami) => {
            if (ami.attributes.id_mentor == userId) {
              return notifications.map((notif) => {
                if (notif.attributes.id_client == ami.attributes.id_patient) {
                  return (
                    <View
                      style={{
                        borderColor: "#15cdfc",
                        borderWidth: 2,
                        marginTop: 20,
                        borderRadius: 20,
                        padding: 10,
                      }}
                    >
                      <Text
                        style={{ textAlign: "center", maxWidth: "100%" }}
                        key={notif.id}
                      >
                        {notif.attributes.titre}
                      </Text>
                      <Text style={{ textAlign: "center" }} key={notif.id}>
                        {notif.attributes.contenu}
                      </Text>
                    </View>
                  );
                }
              });
            }
          })}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    height: 20,
    width: 300,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  scrollView: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
