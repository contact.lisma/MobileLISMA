import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { ScrollView, Text, StyleSheet } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import CustomButton from "../components/CustomButton";
import CustomInput from "../components/CustomInput";
import { COLORS } from "../utils/colors";

export default function ConfirmEmail() {
  const [code, setCode] = useState("");

  const navigation = useNavigation();

  const onConfirmPressed = () => {
    //@ts-ignore
    navigation.navigate("LoggedClient");
  };

  const onSignInPressed = () => {
    //@ts-ignore
    navigation.navigate("SignIn");
  };

  const onResendPressed = () => {
    console.log("Resend");
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.title}>Confirmer votre email</Text>

        <CustomInput
          values={code}
          setValue={setCode}
          placeholder={"Entrez votre code"}
          secureTextEntry={false}
        ></CustomInput>

        <CustomButton
          text="Confirmer l'inscription"
          onPress={onConfirmPressed}
        ></CustomButton>

        <CustomButton
          text="Renvoyer un email"
          onPress={onResendPressed}
          type="SECONDARY"
        ></CustomButton>

        <CustomButton
          text="Revenir à l'inscription"
          onPress={onSignInPressed}
          type="TERTIARY"
        ></CustomButton>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: COLORS.title,
    margin: 10,
    textAlign: "center",
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
});
