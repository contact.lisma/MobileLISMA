import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { ScrollView, Text, StyleSheet } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import CustomButton from "../components/CustomButton";
import CustomInput from "../components/CustomInput";
import { COLORS } from "../utils/colors";

export default function NewPassword() {
  const [code, setCode] = useState("");
  const [newMdp, setNewMdp] = useState("");

  const navigation = useNavigation();

  const onSignInPressed = () => {
    //@ts-ignore
    navigation.navigate("SignIn");
  };

  const onSubmitPressed = () => {
    //@ts-ignore
    navigation.navigate("SignIn");
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.title}>Renitialiser votre mot de passe</Text>

        <CustomInput
          values={code}
          setValue={setCode}
          placeholder={"Code"}
          secureTextEntry={false}
        ></CustomInput>
        <CustomInput
          values={newMdp}
          setValue={setNewMdp}
          placeholder={"Entrez votre nouveau mot de passe"}
          secureTextEntry={false}
        ></CustomInput>

        <CustomButton text="Envoyer" onPress={onSubmitPressed}></CustomButton>

        <CustomButton
          text="Revenir à la connexion"
          onPress={onSignInPressed}
          type="TERTIARY"
        ></CustomButton>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: COLORS.title,
    margin: 10,
    textAlign: "center",
  },
  text: {
    color: "gray",
    marginVertical: 10,
  },
  link: {
    color: "#FDB075",
  },
});
