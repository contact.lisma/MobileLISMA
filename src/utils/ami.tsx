export interface Ami {
  id: number;
  attributes: {
    id_mentor: number;
    id_patient: number;
    createdAt: string;
    updatedAt: string;
  };
}
