export interface Ordonnance {
  id: number;
  attributes: {
    nom_medicament: string;
    id_patient: number;
    nbr_medicament: number;
    heure: string;
    updatedAt: string;
    publishedAt: string;
  };
}
