import React, { createContext } from "react";

export const userContext = createContext({});
export const userIdContext = createContext({});
export const roleContext = createContext({});
export const codeContext = createContext({});
