export interface Notifications {
  id: number;
  attributes: {
    titre: string;
    id_client: number;
    contenu: string;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
  };
}
