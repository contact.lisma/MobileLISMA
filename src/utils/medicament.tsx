export interface Medicaments {
  id: number;
  attributes: {
    nom: string;
    nbr_restant: number;
    need_ordonnance: boolean;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
  };
}
