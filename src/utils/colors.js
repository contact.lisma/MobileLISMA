export const COLORS = {
  textBtn: "#FFFFFF",
  input: "#000000",
  success: "#009E64",
  error: "#EE0017",
  button: "#15cdfc",
  title: "#000000",
};
