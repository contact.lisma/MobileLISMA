export interface Users {
  id: number;
  attributes: {
    nom: string;
    email: string;
    password: string;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    id_structure: string;
    role: string;
    code: number;
  };
}
