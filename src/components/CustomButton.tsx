import React from "react";
import { Text, StyleSheet, Pressable } from "react-native";
import { COLORS } from "../utils/colors";

export default function CustomButton({
  onPress = () => {},
  text,
  type = "PRIMARY",
}: {
  onPress?: () => void;
  text: string;
  type?: string;
}) {
  return (
    <Pressable
      onPress={onPress}
      // @ts-ignore
      style={[styles.container, styles["container_" + type]]}
    >
      <Text
        // @ts-ignore
        style={(styles.button, styles["button_" + type])}
      >
        {text}
      </Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    padding: 15,
    marginVertical: 5,
    alignItems: "center",
    borderRadius: 5,
  },
  container_PRIMARY: {
    backgroundColor: COLORS.button,
  },
  container_SECONDARY: {
    borderColor: "#3B71F3",
    borderWidth: 2,
  },
  container_TERTIARY: {
    color: "gray",
  },
  button: {
    fontWeight: "bold",
    color: COLORS.textBtn,
  },
  button_SECONDARY: {
    color: "#3B71F3",
  },
  button_TERTIARY: {
    color: "gray",
  },
});
