import React from "react";
import { TextInput, View, StyleSheet } from "react-native";
import { COLORS } from "../utils/colors";

export default function CustomInput({
  values,
  setValue,
  placeholder,
  secureTextEntry,
}: {
  values: string;
  setValue: any;
  placeholder: string;
  secureTextEntry: boolean;
}) {
  return (
    <View style={styles.container}>
      <TextInput
        value={values}
        onChangeText={setValue}
        placeholder={placeholder}
        secureTextEntry={secureTextEntry}
        style={styles.input}
      ></TextInput>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginVertical: 15,
  },
  input: {
    color: COLORS.input,
    borderColor: COLORS.input,
    borderWidth: 1,
    height: 40,
    paddingHorizontal: 10,
  },
});
