import React from "react";
import SignIn from "../screens/SignIn";
import SignUp from "../screens/SignUp";
import ConfirmEmail from "../screens/ConfirmEmail";
import MdpForget from "../screens/MdpForget";
import NewPassword from "../screens/NewPassword";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LoggedClient from "../screens/LoggedClient";

const Stack = createNativeStackNavigator();

export default function Navigation() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="SignIn" component={SignIn}></Stack.Screen>
      <Stack.Screen name="SignUp" component={SignUp}></Stack.Screen>
      <Stack.Screen name="ConfirmEmail" component={ConfirmEmail}></Stack.Screen>
      <Stack.Screen name="MdpForget" component={MdpForget}></Stack.Screen>
      <Stack.Screen name="NewPassword" component={NewPassword}></Stack.Screen>
      <Stack.Screen name="LoggedClient" component={LoggedClient}></Stack.Screen>
    </Stack.Navigator>
  );
}
