import React, { useContext } from "react";
import { roleContext } from "../utils/context";

export default function useRole() {
  const { roleUser, setRoleUser } = useContext(roleContext);

  return { roleUser, setRoleUser };
}
