import React, { useContext } from "react";
import { userIdContext } from "../utils/context";

export default function useUserId() {
  const { userId, setUserId } = useContext(userIdContext);

  return { userId, setUserId };
}
