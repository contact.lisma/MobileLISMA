import React, { useContext } from "react";
import { codeContext } from "../utils/context";

export default function useCode() {
  const { codeUser, setCodeUser } = useContext(codeContext);

  return { codeUser, setCodeUser };
}
