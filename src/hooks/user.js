import React, { useContext } from "react";
import { userContext } from "../utils/context";

export default function useUser() {
  const { user, setUser } = useContext(userContext);

  return { user, setUser };
}
